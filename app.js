const { urlencoded } = require('express');
const express = require('express');
const app = express();
const port = 3000;
const fs = require('fs');
const morgan = require('morgan');
const { runInNewContext } = require('vm');
const path = require('path');
const { resourceUsage } = require('process');
let userDb = require('./masterdata/user.json');
const api = require('./api');

// import modules
const router = require('./router');

//middleware
app.use(express.json());
app.use(urlencoded({extended : false}));
app.use(morgan('common'));
app.use(express.static(path.join(__dirname, "public")));

// set
app.set('view engine', 'ejs');

app.use(api);

// =============== ERROR HANDLING ===============
// ***************** INTERNAL ERROR *************
router.use((err, req, res, next) => {
    var error = err.message;
    res.status(500).render('internalError', {error});
})


// ***************** 404 ERROR ******************
router.use((req, res, next) => {
    res.render('404');
})

// =========== MODULE ACTIVATION ============
app.use(router);

// ==============   RUN THE SERVER ==============
app.listen(port, () => {
    console.log('listening on port ' + port)
})



